FROM python:3

WORKDIR /lwtnn

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

ENV PATH=/lwtnn/converters:${PATH}

CMD [ "python", "./your-daemon-or-script.py" ]